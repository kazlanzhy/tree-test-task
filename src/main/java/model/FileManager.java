package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;

public class FileManager implements Runnable {
    private final String NO_MORE_RESULTS = "_DONE_";

    private String mask;
    private int depth;
    private BlockingQueue<String> resultQueue;
    private File rootFile;


    public FileManager(String rootPath, int depth, String mask, BlockingQueue<String> resultQueue) throws FileNotFoundException {

        this.rootFile = new File(rootPath);
        if (!rootFile.exists()) {
            throw new FileNotFoundException("Cannot access " + rootFile.getPath());
        }

        if (!rootFile.isDirectory()) {
            throw new IllegalArgumentException(rootFile.getPath() + " not a directory");
        }

        if (depth < 0) {
            throw new IllegalArgumentException("List depth cannot be less than zero");
        }

        this.depth = depth;
        this.mask = mask;
        this.resultQueue = resultQueue;
    }

    @Override
    public void run() {

        Queue<Entry> queue = new ArrayDeque<>();
        Entry entry = new Entry(0, rootFile);
        File currentFilePath;
        do {
            int nextDepth = entry.getDepth() + 1;
            currentFilePath = entry.getFile();
            if (currentFilePath.isDirectory()) {
                File[] files = currentFilePath.listFiles();

                if (files != null) {
                    for (File file : files) {
                        if (nextDepth <= depth && file.isDirectory()) {
                            queue.offer(new Entry(nextDepth, file));
                        }

                        if (mask == null || file.getName().contains(mask)) {
                            try {
                                resultQueue.put(file.getAbsolutePath());
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            entry = queue.poll();

        } while (entry != null);

        try {
            resultQueue.put(NO_MORE_RESULTS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


}


