package model;

import lombok.Getter;

import java.io.PrintStream;
import java.util.concurrent.BlockingQueue;


public class ResultPrinter implements Runnable {

    @Getter
    private final BlockingQueue<String> resultQueue;
    private final PrintStream printStream;

    private final String DONE = "_DONE_";

    public ResultPrinter(BlockingQueue<String> queue, PrintStream printStream) {
        this.resultQueue = queue;
        this.printStream = printStream;
    }


    @Override
    public void run() {
        String path;
        try {
            printStream.println("");
            while (!(path = resultQueue.take()).equals(DONE)) {
                printStream.print(path + "\r\n");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        printStream.println("\r\nPress any key to continue");
        printStream.flush();
    }


}
