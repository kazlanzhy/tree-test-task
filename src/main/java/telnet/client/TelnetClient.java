package telnet.client;

import model.FileManager;
import model.ResultPrinter;
import telnet.server.TelnetServer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TelnetClient implements Runnable {
    private static Logger logger = Logger.getLogger(TelnetClient.class.getName());
    private final static String PROMPT = "> ";
    private String uniqueId;
    private String currentWorkingDirectory = null;

    private TelnetServer telnetServer;
    private Socket clientSocket;
    private InputStream inputStream = null;
    private OutputStream outputStream = null;
    private boolean stopConnection = false;


    public TelnetClient(TelnetServer telnetServer, Socket clientSocket, String uniqueID) {
        this.telnetServer = telnetServer;
        this.clientSocket = clientSocket;
        this.uniqueId = uniqueID;
    }

    @Override
    public void run() {
        try {
            telnetServer.connectClient(this);
            inputStream = clientSocket.getInputStream();
            outputStream = clientSocket.getOutputStream();

            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            PrintStream pso = new PrintStream(outputStream);
            pso.println("\r\nWelcome to the Kazlanzhy Telnet Server, following commands are available...\r\n");
            pso.println(getHelpOutput());
            pso.print(getPrompt());
            pso.flush();

            String cmd = null;
            while (!stopConnection && (cmd = r.readLine()) != null) {
                pso.println(performTelnetCommand(cmd));
                pso.print(getPrompt());
                pso.flush();
            }
        } catch (SocketException se) {
            return;
        } catch (IOException e) {
            logger.log(Level.WARNING, "An error occurred while handling client input", e);
        } finally {
            telnetServer.clientDisconnected(this);
        }

    }

    /**
     * The method is used to kill the TelnetClient Thread
     * <li> Basically the BufferedReader class methods are  synchronized and blocking IO operations.
     * <li> To overcome this inherent Blocking feature while the execution of thread , we need to explicitly
     * close the socket which are in use, to throw an SocketException to accomplish the Exit of Thread.run()
     */
    public void destroyTelnetClientThread() throws SocketException {
        try {
            clientSocket.close();
        } catch (IOException e) {
            //Actually IOException, but we throw as SocketException
            throw new SocketException();
        }
    }

    /**
     * This method will parse the user input and returns an String[] as command
     *
     * @param cmd User input as single String
     * @return String[]
     */
    private String[] getCommands(String cmd) {
        if (cmd != null) {
            // remove leading & trailing whitespace
            cmd.trim();
            // strip out undesirable spaces e.g. "1 2 3  4    5" "1 2 3 4 5"
            cmd = cmd.replaceAll("[ ]{2,}", " ");

            String[] cmds = cmd.split(" ");
            if (cmds.length > 0) {
                return cmds;
            }
        }
        return null;
    }

    /**
     * This method will run the actual command and return the response
     *
     * @param cmd Command to execute
     * @return Response as String
     */
    private String performTelnetCommand(String cmd) {
        String[] cmds = getCommands(cmd);
        if (cmds != null && cmds.length > 0) {
            String operation = cmds[0];
            if ("ls".equalsIgnoreCase(operation)) {
                return listAllFiles(getCurrentWorkingDirectory());
            } else if ("cd".equalsIgnoreCase(operation)) {
                if (cmds.length > 1) {
                    String dirStr = cmds[1];
                    return changeCurrentWorkingDir(dirStr);
                }
            } else if ("--help".equals(operation)) {
                clear();
                return getHelpOutput();
            } else if ("clear".equals(operation)) {
                clear();
            } else if ("quit".equals(operation)) {
                quit();
            } else if ("filesByMask".equals(operation)) {
                getFilesByMask();
            }
        }
        return "";
    }


    /**
     * This method will change the users current directory
     *
     * @param dirStr
     * @return
     */
    private String changeCurrentWorkingDir(String dirStr) {
        return setCurrentWorkingDirectory(dirStr);
    }

    /**
     * This method will set the current working directory for user sessions
     *
     * @param newWorkDir Required new working directory
     * @return Updated current working directory
     */
    private String setCurrentWorkingDirectory(String newWorkDir) {
        String canonicalPath = getCanonicalPath(newWorkDir);
        File file = new File(canonicalPath);
        if (!file.exists()) {
            return newWorkDir + " - does not exist";
        } else if (!file.isDirectory()) {
            return newWorkDir + " - is not a directory";
        }
        currentWorkingDirectory = canonicalPath;
        return "";
    }

    /**
     * This method will return the help text
     *
     * @return Help menu as string
     */
    private String getHelpOutput() {
        StringBuilder sb = new StringBuilder();
        sb.append("--help              - Display this help menu\r\n");
        sb.append("ls                  - Display all files in current directory\r\n");
        sb.append("cd <DIRECTORY_NAME> - Change the current working directory to the provided arguments\r\n");
        sb.append("filesByMask         - Find all files from rootPath to specified depth by provided mask\r\n");
        sb.append("clear               - Clean the terminal screen\r\n");
        sb.append("quit                - To disconnect\r\n");
        return sb.toString();
    }

    private String listAllFiles(String dirStr) {
        File f = null;
        if (getCurrentWorkingDirectory().equals(dirStr)) {
            f = new File(getCurrentWorkingDirectory());
        } else {
            f = new File(getCanonicalPath(dirStr));
        }
        if (f.exists() && f.isDirectory()) {
            File[] files = f.listFiles();
            StringBuilder sb = new StringBuilder();
            for (File file : files) {
                sb.append(file.getName()).append("\r\n");
            }
            return sb.toString();
        } else {
            return dirStr + " - either it is not a directory or it does not exist";
        }
    }

    private void clear() {
        PrintStream pso = new PrintStream(outputStream);
        pso.println("\033[H\033[2J");
    }

    private void quit() {
        try {
            clientSocket.close();
            stopConnection = true;
        } catch (IOException e) {
            logger.log(Level.WARNING, "An error occurred while closing client connection", e);
        }
    }

    private void getFilesByMask() {
        PrintStream printStream = new PrintStream(outputStream);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String rootPath = setRootPath(bufferedReader, printStream);
        int depth = setDepth(bufferedReader, printStream);
        String mask = setMask(bufferedReader, printStream);

        ResultPrinter resultPrinter = new ResultPrinter(new LinkedBlockingQueue<>(), printStream);
        Thread consumer = new Thread(resultPrinter);

        try {
            FileManager fileManager = new FileManager(rootPath, depth, mask, resultPrinter.getResultQueue());
            Thread producer = new Thread(fileManager);
            producer.start();
            consumer.start();
        } catch (FileNotFoundException | IllegalArgumentException e) {
            printStream.println(e.getMessage());
        }
    }

    private String setRootPath(BufferedReader bufferedReader, PrintStream printStream) {
        printStream.println("\r\nWould you like to use current dir as rootPath (y/n)?: ");
        try {
            String answer = bufferedReader.readLine();
            if (!answer.equalsIgnoreCase("y")) {
                printStream.println("\r\nEnter a rootPath dir: ");
                return bufferedReader.readLine();
            }
        } catch (IOException e) {
            printStream.println("Unable to handle the input, will be used current dir");
        }
        return getCurrentWorkingDirectory();
    }

    private int setDepth(BufferedReader bufferedReader, PrintStream printStream) {
        int depth;
        printStream.println("\r\nEnter your depth: ");
        while (true) {
            try {
                depth = Integer.parseInt(bufferedReader.readLine());
                if (depth < 0) {
                    printStream.println("\t\r\nDepth should be more than list depth cannot be less than zero. Enter again: ");
                } else {
                    return depth;
                }
            } catch (IOException | NumberFormatException e) {
                printStream.println("Unable to handle the input, try again please!");
            }
        }
    }

    private String setMask(BufferedReader bufferedReader, PrintStream printStream) {
        String mask = "";
        printStream.println("\r\nEnter your mask or leave empty and press Enter to continue without it(will show all files): ");
        try {
            mask = bufferedReader.readLine();
        } catch (IOException e) {
            printStream.println("Unable to handle the input, try again please!");
        }
        return mask;
    }


    /**
     * Returns the prompt for user
     *
     * @return Prompt as string to pass to connected client
     */
    private String getPrompt() {
        return getCurrentWorkingDirectory() + PROMPT;
    }

    /**
     * Returns the users current working directory
     *
     * @return
     */
    private String getCurrentWorkingDirectory() {
        if (currentWorkingDirectory == null) {
            currentWorkingDirectory = System.getProperty("user.dir");
        }
        return currentWorkingDirectory;
    }

    /**
     * Unique ID of this thread if required
     *
     * @return
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * This method will parse the user input and return the new working directory
     *
     * @param dirStr New working directory
     * @return parsed working directory
     */
    private String getCanonicalPath(String dirStr) {
        File f = null;
        // First check with root conditions for both platform
        if ((isWindows() && dirStr.startsWith("\\")) ||
                (isWindows() && dirStr.matches("[A-Za-z]:[\\\\/].*")) ||
                (!isWindows() && dirStr.startsWith("/"))) {
            f = new File(dirStr);
        } else {
            // This means user has provided a relative path so start to parse from current working directory
            f = new File(currentWorkingDirectory, dirStr);
        }
        try {
            return f.getCanonicalPath();
        } catch (IOException ioe) {
            return "";
        }
    }

    private boolean isWindows() {
        return telnetServer.getOS().startsWith("Windows");
    }
}
