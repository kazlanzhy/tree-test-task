package telnet.server;

import telnet.client.TelnetClient;

public interface TelnetServer extends Runnable {

    String getOS();

    void clientDisconnected(TelnetClient telnetClient);

    void connectClient(TelnetClient telnetClient);

    String listAllConnections();

    int getNumberOfConnections();

    String disconnectClient(String clientId);

    void disconnectAll();

    void shutDown();
}
