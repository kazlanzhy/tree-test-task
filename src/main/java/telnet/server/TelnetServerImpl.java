package telnet.server;


import telnet.client.TelnetClient;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TelnetServerImpl implements TelnetServer {
    private static Logger logger = Logger.getLogger(TelnetServer.class.getName());

    private int port;
    private AtomicInteger numberOfConnections;
    private Map<String, TelnetClient> activeConnections;
    private String operatingSystem = null;
    boolean shutDownFlag = false;

    private ServerSocket server = null;
    private TelnetClient telnetClient = null;

    public TelnetServerImpl(int port) {
        this.port = port;
        this.numberOfConnections = new AtomicInteger(0);
        this.activeConnections = new HashMap<String, TelnetClient>();
    }

    @Override
    public void run() {
        try {
            server = new ServerSocket(port);

            while (!shutDownFlag) {
                try {
                    // Accept the next connection
                    Socket connection = server.accept();
                    telnetClient = new TelnetClient(this, connection, getUniqueID());
                    new Thread(telnetClient).start();
                }
                //throw SocketException to kill the TelnetServer Thread.
                catch (SocketException ee) {
                    break;
                }
            }
        } catch (InterruptedIOException e) {
            logger.log(Level.FINE, "This can be considered a normal exit", e);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Unable to accept connections", e);
        }
    }

    @Override
    public int getNumberOfConnections() {
        return numberOfConnections.intValue();
    }

    /**
     * This will called by client once its started successfully
     */
    @Override
    public void connectClient(TelnetClient clientObj) {
        addToActiveConnection(clientObj);
        numberOfConnections.addAndGet(1);
    }

    public String getOS() {
        if (operatingSystem == null) {
            operatingSystem = System.getProperty("os.name");
        }
        return operatingSystem;
    }

    /**
     * This method will be called to updated list of connected clients
     */
    @Override
    public void clientDisconnected(TelnetClient clientObj) {
        removeFromActiveConnection(clientObj);
        numberOfConnections.decrementAndGet();
    }


    @Override
    public String disconnectClient(String clientId) {
        if (clientId != null) {
            TelnetClient clientToKill = activeConnections.get(clientId);
            if (clientToKill != null) {
                try {
                    clientToKill.destroyTelnetClientThread();
                    clientDisconnected(clientToKill);
                } catch (SocketException se) {
                    logger.log(Level.SEVERE, "Failed to disconnect client " + clientId + " - " + se.getMessage());
                    return "Failed";
                }
                return "Success";
            } else {
                return "Invalid client id provided";
            }
        } else {
            return "Client id is mandatory input";
        }
    }

    @Override
    public void disconnectAll() {
        Iterator it = activeConnections.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            disconnectClient((String) pairs.getKey());
        }
        System.out.println("\n\rAll clients disconnected");
    }

    @Override
    public void shutDown() {
        disconnectAll();
        try {
            server.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to stop server - " + e.getMessage());
        } finally {
            shutDownFlag = true;
        }

    }

    /**
     * Return the list of active connection as String
     */
    @Override
    public String listAllConnections() {
        Iterator it = activeConnections.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        sb.append("Client ID					                Client Object\n");
        sb.append("=========					                =============\n");
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            sb.append(pairs.getKey()).append("		").append(pairs.getValue().toString()).append("\n");
        }
        sb.append("Active Connections = ").append(getNumberOfConnections());
        sb.append("\n");
        return sb.toString();
    }

    private String getUniqueID() {
        return (UUID.randomUUID().toString());
    }

    private void removeFromActiveConnection(TelnetClient clientObj) {
        activeConnections.remove(clientObj.getUniqueId());
    }

    private void addToActiveConnection(TelnetClient clientObj) {
        activeConnections.put(clientObj.getUniqueId(), clientObj);
    }

}
