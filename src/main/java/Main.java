import telnet.server.TelnetServer;
import telnet.server.TelnetServerImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main {

    public static int DEFAULT_SERVER_PORT = 7777;

    public static void main(String[] args) {

        TelnetServer server = new TelnetServerImpl(DEFAULT_SERVER_PORT);
        new Thread(server).start();

        System.out.println("Telnet server started successfully on port " + DEFAULT_SERVER_PORT);


        String currentLine = "";
        InputStreamReader converter = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(converter);
        while (true) {
            System.out.println(getMenu());
            try {
                currentLine = in.readLine();
            } catch (IOException ioe) {
                System.out.println("Unable to handle the input, try again please!\n");
            }

            switch (currentLine) {
                case "1":
                    System.out.println("Active connections are as follows;\n");
                    System.out.println(server.listAllConnections());
                    break;
                case "2":
                    System.out.println("|____ Please enter the connection id to disconnect: ");
                    try {
                        currentLine = in.readLine();
                        System.out.println(server.disconnectClient(currentLine));
                    } catch (IOException ioe) {
                        System.out.println("Unable to handle the input, try again please!");
                    }
                    break;
                case "3":
                    server.disconnectAll();
                    break;
                case "4":
                    System.out.println("****** This action will disconnect all connected clients and shutdown the server, do you want to continue (y/n)?");
                    try {
                        currentLine = in.readLine();
                        if (currentLine.equalsIgnoreCase("y")) {
                            server.shutDown();
                            return;
                        } else {
                            System.out.println("......Action Aborted!");
                        }
                    } catch (IOException ioe) {
                        System.out.println("Unable to handle the input, try again please!");
                    }
                    break;
                default:
                    System.out.println("Option '" + currentLine + "' is invalid, try again!\n");
                    break;
            }
        }
    }


    /**
     * Main Menu
     *
     * @return Menu as string
     */
    private static String getMenu() {
        StringBuilder sb = new StringBuilder("\n");
        sb.append("1. List all active clients\n");
        sb.append("2. Disconnect a single client\n");
        sb.append("3. Disconnect all clients\n");
        sb.append("4. Shutdown server and quit\n");
        sb.append("> ");
        return sb.toString();
    }

}
